# Come modificare il sito


* Clicca sul pulsante "Web IDE"

![](readme/web_ide.png)

* Sulla sinistra trovi un elenco di cartelle: sotto `content` trovi tutte le pagine del sito
* Entra in una pagina e modificane il contenuto usando la [sintassi markdown](https://www.markdownguide.org/cheat-sheet/)

![](readme/editor.png)

* Durante la modifica puoi vedere un'anteprima nella scheda *preview Markdown*. L'anteprima non è identica a come sarà poi sul sito, ma un'idea te la fai.

![](readme/preview.png)

* Una volta finite le modifiche devi salvare cliccando il pulsante blu *Commit...*
* **importante:** nella schermata di salvataggio (commit) bisogna ricordarsi di selezionare **Commit to master branch**!

![](readme/commit.png)

* Una volta effettuato il commit, parte in automatico la rigenerazione del sito. Non è istantanea, c'è un'attesa di 5 minuti circa.
* Il sito che viene generato è qua: https://giorgioperna-2.gitlab.io/ijf2022_retejo/


---
title: "Tema"
---
Quest'anno il festival si terrà in montagna, a Serrada di Folgaria (TN), e come forse già sapete, ogni anno il nostro festival ha un tema diverso. 
Il tema di quest'anno è **I ponti al di là dei monti**. 

L'evento di quest'anno avrà come obiettivo quello di celebrare e promuovere le pratiche sociali che costruiscono connessioni, abbattendo barriere e colmando le distanze tra persone e popoli. Insieme rifletteremo sul potere delle azioni quotidiane e delle iniziative sociali nel favorire la pace, la collaborazione e l'empatia. Saranno questi i ponti invisibili su cui cammineremo nell'arco della settimana che trascorreremo in compagnia. 

Gli scorsi due anni sono stati dei momenti di riaccensione di conflitti in varie parti del mondo, tra cui quello in Ucraina e da qualche mese anche la guerra nel vicino Medio Oriente. Siamo convinti che fra le ragioni fondamentali della nascita di tali conflitti sia necessario evidenziare anche la mancanza di una "cultura dell'interculturalismo", ovvero dell'assenza di comunicazione e di scambio culturale tra realtà prossime l'una all'altra. La mancanza di politiche mirate all'inclusione, all'integrazione e alla condivisione è anche uno dei motivi principali delle difficoltà che abbiamo nell'affrontare politicamente la questione dell'immigrazione, tanto in Italia quanto in Europa. 

In un mondo così diviso da differenze culturali, politiche e sociali, il nostro festival, tramite l'ausilio dell'Esperanto, si propone di sottolineare le molteplici forme di connessione che possiamo creare per superare queste divisioni. I ponti a cui ci riferiamo noi non sono i classici ponti in mattoni o in legno che uniscono le rive dei fiumi: si tratta di ponti metaforici, di ponti invisibili, quelli più difficili da individuare.

Tuttavia, un po' come accade nel dipinto "Il ponte di Eraclito" di Magritte, il potere immaginativo è capace di intravedere altre realtà lì dove il mondo sembra finire. E spesso è quanto basta per cominciare a costruire una nuova idea di mondo.

Attraverso una varietà di attività, spettacoli, discussioni e workshop, esploreremo come ogni individuo può diventare un architetto di ponti invisibili, ponti al di là dei monti.

Nelle prossime settimane sveleremo il programma del festival. Seguici per rimanere aggiornato e unisciti a noi per portare avanti questo progetto!


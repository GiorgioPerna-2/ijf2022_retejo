---
title: "Contatti"
---
Per informazioni sul Festival:

* Posta elettronica: iej@esperanto.it;
* Instagram: https://www.instagram.com/iejitalio/
* Evento Facebook: https://fb.me/e/1d8aUte5C 
* Gruppo Telegram: https://t.me/ijf2022

Per informazioni sull’associazione:

* Posta elettronica: iej@esperanto.it;
* Sito web: [iej.esperanto.it](https://iej.esperanto.it/);
* Pagina Facebook: [Gioventù Esperantista Italiana – Itala Esperantista Junularo](https://www.facebook.com/IEJesperanto/).

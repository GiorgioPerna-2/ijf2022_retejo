---
title: "Festivalejo"
---

IJF 2024 estos gastigata en la **Hotel Lares**, eta kaj ĉarma hotelo, je nia plena disponeblo, kiu situas en la centro de la vilaĝo Serrada (komunumo de Folgaria) en la valo Adige. 500 metrojn for de la hotelo troviĝas skilifto por iri skii, kaj cirkaŭe tuj atingeblaj estas montarvojetoj kaj mirindaj panoramoj.

La hotelo situas en *Via Enrico Fermi 44, 38064 Serrada di Folgaria (TN)*.

## Pliaj informoj

Ĉiuj manĝoj de IJF estos **nur vegetaraj aŭ veganaj**. Kompreneble, estos tute permesate por partoprenantoj kunporti kaj manĝi viandaĵojn en la Festivalejo.

En la tuta Festivalejo **hejmbestoj estas malpermesataj**, kaj **oni ne rajtas fumi ene de la konstruaĵoj**.

La **litoj devas esti uzataj kun littukoj**: vi povas aŭ kunporti viajn proprajn littukojn, aŭ uzi la surlokajn. Uzi dormosakon en la lito estas malpermesite. 

La organizantoj ne surprenas respondecon por damaĝoj al sano aŭ al posedaĵoj de partoprenantoj, kiuj okazas dum la aranĝo. La kostoj de eventualaj damaĝoj al la Festivalejo kaŭzitaj de partoprenantoj estos kovritaj de tiuj partoprenantoj mem, kiuj kaŭzis la damaĝojn.

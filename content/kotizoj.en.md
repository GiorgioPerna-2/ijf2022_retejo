---
title: "Fees"
---
## Down payments
In order for the registration to be valid, the IEJ must receive a down payment.
The **minimum** down payment for the IJF 2023 is **30 €**, or the entire amount if the amount to be paid is less than this amount.

After receiving the deposit, a confirmation will be sent on the availability of places: only with this confirmation will the registration be valid and will become official.

Upon arrival at the festival venue, you will need to show proof of your down payment and ID.

In case of **registration withdrawal**, the minimum deposit will not be refunded. If the amount paid as an advance is higher than the minimum threshold, then you will be entitled to a refund of the amount net of the down payment, provided that the absence has been communicated by 03/20/2023.

You can **transfer your registration** to another participant, provided you present a written document certifying the transfer; however the fee will be adjusted according to the new participant (period/age/country). Multiple accounts cannot be transferred to the same person.


## Odds
The basic fees of the festival vary on the basis of three parameters, explained below:

- **Country of residence:** The states are divided into four groups according to Gross Domestic Product and distance. The respective shares decrease from the first to the fourth group:
    1. Andorra, Austria, Belgium, Catalonia, Vatican City, Denmark, Finland, France, Germany, Ireland, Italy, Lichtenstein, Luxembourg, Norway, Netherlands, Principality of Monaco, United Kingdom, San Marino, Sweden, Switzerland.
    2. Cyprus, Greece, Iceland, Malta, Portugal, Czech Republic, Slovakia, Slovenia, Spain.
    3. Australia, Brunei, Canada, Croatia, United Arab Emirates, Estonia, Japan, Hong Kong, Hungary, Israel, Kuwait, Latvia, Lithuania, Qatar, New Zealand, Poland, Singapore, United States.
    4. All other states.

- **Enrollment period:** Two enrollment periods are considered:
    1. By 01/03/2023
    2. From 02/03/2023 until the start of the festival
In the second period the share is higher than in the first.

- **Age:** The age of participants is divided into four categories, with rates increasing with age. Children under 3 do not pay the fee.
Age is calculated on the first day of the festival, which this year will be 05/04/2023.

We also remind you that participants who reside in Italy and who are not members of the Italian Esperantist Federation (FEI)) will have to pay an additional sum, which is not included in the fee calculated in the aliĝilo, as explained below:

**Non-Members**
| Participant | Age | Amount payable |
| --- | --- | --- |
| Ordinary | 25+ | €32 |
| Young | 0 - 24 | €16 |

Enrollment in the FEI can be done directly upon arrival at the festival site; the cost is as follows:

**FEI Membership**
| Member | Age | Amount to be paid |
| --- | --- | --- |
| Ordinary | 25+ | €30 |
| Young | 0-25 | €15 |

All FEI members will be required to prove that they have paid the membership fee to the association.

**Countries of origin 1**
| Age \\ Enrollment period | 1 | 2 |
| ----------------- | --- | --- |
| **3-18** | 257 | 272 |
| **19-26** | 282 | 297 |
| **27-35** | 302 | 317 |
| **36+** | 352 | 367 |

**Countries of origin 2**
| Age \\ Enrollment period | 1 | 2 |
| ----------------- | --- | --- |
| **3-18** | 247 | 262 |
| **19-26** | 272 | 287 |
| **27-35** | 292 | 307 |
| **36+** | 342 | 357 |

**Countries of origin 3**
| Age \\ Enrollment period | 1 | 2 |
| ----------------- | --- | --- |
| **3-18** | 237 | 252 |
| **19-26** | 262 | 277 |
| **27-35** | 282 | 297 |
| **36+** | 332 | 347 |
 
**Countries of origin 4**
| Age \\ Enrollment period | 1 | 2 |
| ----------------- | --- | --- |
| **3-18** | 227 | 242 |
| **19-26** | 252 | 267 |
| **27-35** | 272 | 287 |
| **36+** | 322 | 337 |

The rates shown above are the basic ones, for the complete stay during the 6 days. They include: accommodation, meals (from dinner on April 5 to breakfast on April 11) and participation in the festival programme. They *do not* include the tourist tax of €1.50 per day (for a maximum of €6) which the Calabria region requires of every person over the age of 11

In case of **stay for less than 6 days**, the daily rate is **one fifth** of the basic rate.

The additional services we offer have the following costs:

| Service | Price |
| --- | --- |
| Room for 3/4 people | 5 € / day |
| Double room | 15 € / day |
| Single room | 35 € / day |
| Additional Meals | €15 |
| Invitation letter (if you are under 30) | €5 |
| Invitation letter (if you are over 30) | €10 |

** ATTENTION: Invitation letters must be requested as soon as possible due to bureaucratic times.
It is strongly recommended that those requesting the letter of invitation pay the full amount in advance, so that it is more likely that they will obtain a visa. In case of non-obtainment of the visa, the difference between the total fee and the deposit due will be refunded.**

## Discounts
Four different types of discounts are established for the IJF (in addition to those deriving from scholarships and other agreements):
    1. **Contribution to the program:** Concerns participants who propose and will manage activities for the program (theme related or not). Usually the discount is €10 for each hour of contribution;
    2. **Collaboration in the organization:** Concerns the participants who will help the organizers with the management activities of the festival (trinkejo, gufujo, etc). The discount in this case is decided on a case-by-case basis;
    3. **IEJ Supporters:** Concerns supporter members of the IEJ. The discount is 10%;
    4. **Group discount:** Discount that applies to participants who will come through a local Esperantist group or in a group of at least 6 people. It varies according to the number of group members.
   
For further information and agreements, contact us at iej@esperanto.it.

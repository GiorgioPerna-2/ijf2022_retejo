---
#title: "Aliĝilo"
---
Por aliĝi al la festivalo, alklaku [ĉitie](https://shorturl.at/hiwH3)
<!-- 
questo è il link esteso: 
https://docs.google.com/forms/d/1UhC1WtnypgTDUZltBlw7ZwouD50gSSAkwTAHqzF_i1c/edit
-->
Aliĝu tuj! Memoru: ju pli frue vi aliĝos, des pli vi ĝojos 😉

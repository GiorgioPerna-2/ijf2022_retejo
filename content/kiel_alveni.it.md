---
title: "Come arrivare"
---
## Sede del Festival
* **Nome** Hotel Lares
* **Indirizzo** Via Enrico Fermi 44, 38064 Serrada di Folgaria (TN)
* **Coordinate** 45°53’53.7”N - 11°9’21.4”E
* **Numero di telefono** +39 0464 727129
* **Indirizzo mail** lares@mhhotels.eu

## Come arrivare
In questa pagina presentiamo alcune possibilità per raggiungere la sede del festival.
Consultate i seguenti siti per avere maggiori informazioni e orari aggiornati:

* [Skyscanner](https://www.skyscanner.it/) (per gli aerei)
* [Trenitalia](https://www.trenitalia.com/) (per i treni)

Gli itinerari seguenti sono i più consigliati per il tempo, la distanza e il costo. Tranne se indicato il contrario, essi sono validi sia per l'andata che per il ritorno

### {{<mdi mdi-airplane>}} Con l'aereo
Gli aeroporti più vicini sono quelli di Venezia, Treviso, Verona, Bergamo e Bolzano.

### {{<mdi mdi-train>}} Con il treno o il bus
La città più grande vicino a Serrada è Rovereto (TN). E' facilmente raggiungibile sia in bus che in treno (si trova sulla linea Bologna- Brennero).
Da Rovereto ci sono autobus oer raggiungere sia direttamente che indirettamente Serrada, dove si trova la sede del nostro Festival.

<!--Ĉi tie la retejo pri bushoroj: LINK ORARI ROVERETO-SERRADA-->

In caso di bisogno per altri consigli o informazioni su come raggiungere il Festival, contattateci a iej@esperanto.it !

### {{<mdi mdi-car>}} Con l'auto
Dai, nel 2024 possiedi sicuramente un telefono con il navigatore... Davvero stai leggendo qui? 🙂
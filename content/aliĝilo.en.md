---
#title: "Registration form"
---
To register for the festival, [click here](https://www.shorturl.at/fNUWZ).

<!-- 
questo è il link esteso: 
https://docs.google.com/forms/d/e/1FAIpQLSeplf1Vd4EjenMqryxSv4Np7kbNsjpZMwC3eyNkUwhnhCjBTw/viewform?usp=pp_url&entry.371642562=5-lita+%C4%89ambro+-+Quintupla:+jam+inkluzivita+en+la+kotizo/inclusa+di+base+nell%27iscrizione&entry.1415765222=Ne+gravas+-+Mi+%C3%A8+indifferente&entry.660018025=Ne+-+No&entry.810971066=Ne+-+No&entry.511024878=Mi+man%C4%9Das+%C4%89ion+ajn,+e%C4%89+tablon!+-+Mangio+tutto,+anche+il+tavolo!&entry.2011128736=Ne+-+No&entry.304587619=Bank%C4%9Diro+-+Bonifico+(IT76T0306909606100000131970)+al+-+a+%22FEI+-+Federazione+Esperantista+Italiana%22+skribante+-+scivendo+%22*VIA+NOMO*+pago+por+IJF+2023+en+Palmi%22&entry.2087436947=Ne+(%C4%89u+vere??)+-+No+(veramente??)&entry.1413058979=Jes+-+S%C3%AC&entry.928648563=Jes+-+S%C3%AC&entry.803710754=Jes+-+S%C3%AC
-->
Register now! And remember: the earlier you sign up, the more you can look forward to the event 😉 😉


---

---
**Sabato 30 Marzo si terrà la gita a Trento!** 

Trento è una città nel nord Italia, nella regione Trentino-Alto Adige/Südtirol, con circa 117.000 abitanti. 

Il suo antico nome "Tridentum" deriva probabilmente dai tre monti (Bondone, Calisio, Marzola) o dalle tre colline (Sant'Agata, San Rocco, Verruca) che circondano la città.

La città possiede una ricca storia che inizia dall'epoca romana
Trento è conosciuta per la sua diversità culturale, per l'Università e per la sua architettura. La città ha un centro storico ben conservato, che include la Cattedrale di San Viglio e il Castello del Buonconsiglio. 

Il **Castello del Buonconsiglio** è il monumento più importante della regione.
Fortezza della metà del 13esimo secolo, dimora dei principi vescovi di Trento fino all'epoca napoleonica, poi caserma e adesso museo, si presenta come formato da tre parti principali, costruite in epoche differenti.
Da visitare per i suoi begli affreschi e per la vista panoramica sulla città.

Attornoa  Trento si trovano belle **montagne e laghi**, con un paesaggio spettacolare. La regione è famosa tra i turisti per le montagne, i sentieri, lo sci e le altre attività all'aperto.
La regione offre diverse specialità gastronomiche, tra cui formaggi vini e dolci (ad esempio il tradizionale strudel). 

Inoltre, avremo la possibilità di conoscere e incontrare gli **esperantisti locali**, per conoscere meglio la vita in queesta splendida città!

![kastelo1](https://iej.esperanto.it/ijf/2024/kastelo1.jpg)
<img src = "https://iej.esperanto.it/ijf/2024/kastelo1.jpg" alt="kastelo1">

![kastelo2](https://iej.esperanto.it/ijf/2024/kastelo2.jpg)
<img src = "https://iej.esperanto.it/ijf/2024/kastelo2.jpg" alt="kastelo2">



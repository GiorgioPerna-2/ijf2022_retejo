---
title: "Theme programme"
---

The IJF theme this year is **The baton of Esperanto among the youth sections**.
Through this theme we want to introduce our partecipants to the current diffusion 
of Esperanto among the youth of all the world.
Through this theme we want to introduce our participants to the current diffusion of Esperanto among young people from all over the world.

During the IJF there will be a chance to speak directly with members of TEJO's youth sections, who have been most successful in strengthening or enlarging their country's Esperantist movement in recent years, to find out how they did it.
At the same time, the goal is also to inspire other young people to do the same.

In addition to **TEJO** (*Tutmonda Esperantista Junulara Organizo*), the following youth associations will speak on the subject, which will tell their own experience we have:
- **LEJO** (*Litova Esperanta Junulara Organizo*)
- **ALEJ** (*Aotearoa Ligo de Esperantistoj Junaj*)
- **KEJ** (*Kataluna Esperanto-Asocio*)

[Here you can find the thematic program](http://shorturl.at/qrFW9)
---
title: "Modalità di pagamento"
---
## Bonifico bancario
È il metodo più utilizzato per trasferire somme di denaro da un conto corrente a un altro.

Il pagamento va effettuato a:

- Banca: Intesa San Paolo
- Titolare: Federazione Esperantista Italiana
- IBAN: IT76T0306909606100000131970
- BIC: BCITITMM.

È necessario indicare nella causale “IJF 2024 – NOME DELL'ISCRITTO”.

## PayPal
Uno dei metodi più usati per trasferire somme di danaro via internet. Creare un conto è molto semplice.

Per pagare è sufficiente entrare nel proprio conto PayPal, scegliere l'opzione "Invia denaro" e inserire i seguenti estremi di pagamento:

- Indirizzo e-mail: iej.financoj@esperanto.it
- Somma: La somma da trasferire (acconto o saldo) in euro
- Acquisto di: Servizi.

Nello spazio "Messaggio al ricevente” inserire “IJF 2024 – NOME DELL'ISCRITTO”.

**ATTENZIONE: In caso di pagamento via PayPal, IEJ ha una spesa aggiuntiva per il servizio (3,4%+0,35€), che sarà addebitata al partecipante al momento della registrazione sul posto.**

## Carta di credito
Tramite UEA è possibile pagare attraverso carta di credito al conto IEJ. È sufficiente entrare nella pagina [https://db.uea.org/alighoj/spagilo.php](https://db.uea.org/alighoj/spagilo.php), inserire il codice IEJ (ieja-z), scrivere “Alia celo” nella casella “Pago-celo”, kaj precisare la causale di pagamento: “IJF 2024 – NOME DELL'ISCRITTO”.

**ATTENZIONE: In caso di pagamento con carta di credito, la IEJ ha una spesa aggiuntiva per il servizio (5%), che sarà addebitata al partecipante al momento della registrazione sul posto.**

## Conto UEA
Per chi ha un conto presso l'Associazione universale di esperanto è possibile inviare danaro alla IEJ tramite messaggio di posta elettronica a financoj@co.uea.org (mettendo in copia iej@esperanto.it), indicando il proprio codice di conto personale e il codice del ricevente, cioè la IEJ (ieja-z). 

**ATTENZIONE: dopo il 10/03/2024 non sarà più possibile pagare con questo metodo.**

## Contanti
È anche possibile pagare l'acconto in contanti, specificando la somma versata e il ricevente.

Sarà anche possibile pagare in contanti il saldo all'arrivo al festival, al momento della registrazione e dell'assegnazione della stanza.

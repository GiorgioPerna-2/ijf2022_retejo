---
#title: "Festival giovanile internazionale di esperanto 2024"
type: homepage
---
Anche nel 2024 si svolgerà l’IJF!
Il **Festival Giovanile Internazionale** (in esperanto “**Internacia Junulara Festivalo**”, o IJF), organizzato dalla Gioventù esperantista italiana (IEJ), è il più importante evento giovanile italiano e si svolge durante il periodo di Pasqua.

Quest’anno il Festival ci porterà in **Trentino Alto Adige**, regione piena di possibilità per gli amanti dello sport e della natura, per buongustai e amanti della cultura, vi offrirà senza dubbio un soggiorno indimenticabile.

La località che ci ospiterà è **Serrada di Folgaria**, un posto molto interessante, vicino alla bella città di **Trento**.

Come sempre il Festival si terrà durante la settimana di Pasqua, **dal 27 marzo al 2 aprile 2024** e avrà un programma molto vario, che si distingue solitamente in tre diverse **categorie**:

* **Programma tematico:** Creato attorno al tema scelto dalla IEJ per il festival e ricco di conferenze, gruppi di lavoro e seminari;
* **Programma turistico:** Due escursioni, una di mezza giornata e una di una giornata intera, per far conoscere ai partecipanti il meglio della regione che li ospita;
* **Programma ricreativo:** Attività varie, consistenti in tutto ciò non direttamente correlato al programma principale, ad esempio la serata internazionale (in cui ogni partecipante può presentare qualcosa di tipico del suo Paese), giochi, corsi di danza popolare, concerti.

Un'importante occasione per **partecipare quasi gratis** è attraverso i progetto di TEJO, come spiegato in dettaglio qui: https://www.tejo.org/it/partoprenanto-seminario-ijf2024/?fbclid=IwAR1b22uBfv5LuJJNFNlDS-SPI5Cn-olESmh63KMRm7CPNkZv8MA1CltC_LA


![projekto](https://iej.esperanto.it/ijf/2024/projekto.png)
<img src = "https://iej.esperanto.it/ijf/2024/projekto.png" alt="projekto">
---
#title: "This is the International Esperanto Youth Festival 2023"
type: homepage
---

The IJF will take place again in 2023!
The **International Youth Festival** (in Esperanto “**Internacia Junulara Festivalo**”, or IJF), organized by the Italian Esperantist Youth (IEJ), is the most important Italian youth event and takes place during the Easter period.

Despite the UK and IJK taking place in Italy in the summer, we thought we'd keep the tradition of this festival alive, which this year will take you to a region that has never hosted the IJF: **Calabria!**. A region full of possibilities for sports and nature lovers, gourmets and culture lovers, it will undoubtedly offer you an unforgettable stay.

The city that will host us is Palmi: located in the south of the region, in the province of Reggio Calabria, it rises on the Tyrrhenian Sea and close to the slopes of Monte Sant'Elia, making it ideal for both beach lovers and more intrepid travellers!

During the Easter week, **from 5 to 11 April 2023**, we will be guests of the **Donna Canfora** holiday village.

Like every year, the International Youth Esperanto Festival has a very varied program, which is usually divided into three different **categories**:

* **Thematic programme:** Created around the theme chosen by the IEJ for the festival and packed with conferences, workshops and seminars;
* **Tourist program:** Two excursions, one half day and one full day, to introduce participants to the best of the region that hosts them;
* **Recreational program:** Various activities, consisting of everything not directly related to the main program, for example the international evening (where each participant can present something typical of his country), games, folk dance classes, concerts .

 
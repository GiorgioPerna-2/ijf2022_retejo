---
title: "La sede"
---

L'IJF 2024 verrà ospitato nell'**Hotel Lares**, un piccolo hotel a nostra completa disposizione, che si trova al centro del paese di Serrada, nel comune di Folgaria (TN). A 500 metri dall'hotel si trova lo skilift per andare a sciare, e subito raggiungibili a piedi ci sono sentieri di montagna e panorami spettacolari.

L'hotel si trova in *Via Enrico Fermi 44, 38064 Serrada di Folgaria (TN)*.

## Ulteriori informazioni

Tutti i pasti dell'IJF saranno solamente **vegetariani o vegani**. Ovviamente sarà permesso ai partecipanti portare con sè e mangiare carne nella struttura.

In tutta la struttura **gli animali sono vietati** e **non è permesso fumare dentro l'edificio**.

**I letti devono essere usati con le lenzuola**: potete portarle con voi oppure pagare utilizzare quelle già presenti. Usare sacchi a pelo sui letti è vietato.

Gli organizzatori non hanno responsabilità per danni alla salute o ai beni dei partecipanti. Il costo di eventuali danni alla struttura cauzati da partecipanti saranno coperti dagli stessi partecipanti che li hanno causati.

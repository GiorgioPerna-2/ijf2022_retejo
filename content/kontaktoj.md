---
title: "Kontaktoj"
---
Por pliaj informoj pri la Festivalo:

* Retpoŝto: iej@esperanto.it;
* Instagram: https://www.instagram.com/iejitalio/
* Facebook-evento: https://fb.me/e/1d8aUte5C
* Telegrama grupo: https://t.me/ijf2022


Por informoj pri nia asocio:

* Retpoŝto: iej@esperanto.it;
* Retejo: [iej.esperanto.it](https://iej.esperanto.it/wordpress/eo/);
* Facebook-paĝo: [Gioventù Esperantista Italiana – Itala Esperantista Junularo](https://www.facebook.com/IEJesperanto/)

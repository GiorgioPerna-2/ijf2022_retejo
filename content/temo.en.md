---
title: "Theme"
---
This year the festival will be held in the mountains, in Serrada di Folgaria (TN), and as you may already know, every year our festival has a different theme. 
This year's theme is **Bridges Across the Mountains**. 

This year's event will aim to celebrate and promote social practices that build connections, breaking down barriers and bridging distances between people and populations. 
Together we will reflect on the power of daily actions and social initiatives in fostering peace, collaboration and empathy.  
These will be the invisible bridges over which we will walk during the week we will spend in company.
The past two years have been moments of rekindling of conflicts in various parts of the world, including that in Ukraine and in recent months also the war in the nearby Middle East.  

We are convinced that among the fundamental reasons for the emergence of such conflicts it is also necessary to highlight the lack of a "culture of interculturalism", that is, the absence of communication and cultural exchange between realities close to each other. 
The lack of policies aimed at inclusion, integration and sharing is also one of the main reasons for the difficulties we have in politically addressing the issue of immigration, both in Italy and in Europe.

In a world so divided by cultural, political and social differences, our festival, through the help of Esperanto, aims to underline the multiple forms of connection that we can create to overcome these divisions.  The bridges we are referring to are not the classic brick or wooden bridges that connect the banks of rivers: they are metaphorical bridges, invisible bridges, the most difficult to identify.

However, a bit like what happens in Magritte's painting "The Bridge of Heraclitus", the imaginative power is capable of glimpsing other realities where the world seems to end.  And often it is enough to start building a new idea of ​​the world.

Through a variety of activities, performances, discussions and workshops, we will explore how each individual can become an architect of invisible bridges, bridges beyond the mountains.

In the next few weeks we will reveal the festival program.  Follow us to stay updated and join us to carry this project forward!
---
title: "Temo"
---
Ĉi-jare la festivalo okazos tra la montoj, en Serrada di Folgaria (TN) kaj kiel eble vi jam scias, nia festivalo havas malsaman temon ĉiun jaron. 
La temo de ĉi tiu jaro estas **La pontoj trans la montoj**.

La celo de la evento ĉi-jare estos celebri kaj promocii sociajn praktikojn kiuj konstruas konektojn, rompas barojn, kaj plenigas distancojn inter homoj kaj popoloj. Pripensante kune la potencon de la ĉiutagaj agoj kaj de la sociaj iniciatoj por favori pacon, kunlaboron kaj empation, ni piedirados sur tiuj nevideblaj pontoj dum la semajno, kiun ni pasigos kune.

La lastaj du jaroj estis periodo de reeekflamo de konfliktoj en diversaj partoj de la mondo, inkluzive de tiu en Ukrainio kaj, ekde kelkaj monatoj, ankaŭ de la milito en la proksima Mezoriento. Ni opinias, ke inter la fundamentaj kialoj de naskiĝo de tiaj konfliktoj oni devas konsideri ankaŭ la mankon de "interkultura kulturo", t.e., la mankon de komunikado kaj de kultura interŝanĝo inter proksimaj realoj. La manko de politikoj celantaj inkluzivecon, integracion, kaj kunhavigon estas ankaŭ unu el la ĉefaj kialoj de la malfacilaĵoj kiujn ni spertas politike en la temo de imigrado, tiom en Italio, kiom en Eŭropo.

En mondo tiel disigita pro kulturaj, politikaj kaj sociaj diferencoj, nia festivalo, per la helpo de Esperanto, celas substreki la multajn formojn de konektoj kiujn oni povas krei por superi tiujn disigojn. La pontoj al kiuj ni rilatas ne estas la kutimaj pontoj el brikoj aŭ ligno, kiuj kunligas la bordojn de riveroj: ili estas metaforaj pontoj, pontoj nevideblaj, tiuj plej malfacile identigeblaj.

Tamen, iom simile al la pentraĵo "La ponto de Heraklito" de Réné Magritte, la imagpovo kapablas ekvidi aliajn realaĵojn tie kie la mondo ŝajnas finiĝi. Kaj ofte tio sufiĉas por komenci konstrui novan ideon pri la mondo!

Per variaj agadoj, spektakloj, diskutoj kaj labortagoj, ni esploros kiel ĉiu individuo povas fariĝi arkitekto de nevideblaj pontoj, pontoj preter la montoj.

Dum la venontaj semajnoj ni malkaŝos la programon de la festivalo. Sekvu nin por resti informita kaj aliĝu al ni por subteni tiun ĉi projekton!



![Ponto](https://iej.esperanto.it/ijf/2024/Ponto.jpeg)
<img src = "https://iej.esperanto.it/ijf/2024/Ponto.jpeg" alt="Ponto">
---
title: "Listo de la aliĝintoj"
---

| **Aliĝinto** | **Lando** |
| --- | --- |
| Giovanni De Lucia | Italio |
| Diego Filippo Lotesto | Italio |
| Gabriele Esposito Guido | Italio |
| Giulia Costanzi | Italio |
| Fabian Sambrano Aguilar | Italio |
| Petr Javorik | Ĉeĥujo |
| Vlasta Pištíková | Ĉeĥujo |
| Azadi Mhdjou | Komoraj Insuloj |
| Lars Sözüer | Svisujo |
| Mattia Ciceri | Italio |
| Utku Özdemir | Italio |
| Aušrinė	Tamošiūnaitė | Hispanujo |
| Vittorio	Bignami | Italio |
| *kaŝita* | 
| *kaŝita* |
| Jack	Fordon | Francujo |
| Aida	Čižikitė | Litovujo |
| Liudvikas	Medzevičius | Litovujo |
| Glorija Elžbieta	Lukaitė | Litovujo |
| Pol	Jimenez Isach | Andoro |
| *kaŝita* | 
| *kaŝita* |
| Miglė	Jančaitytė| Litovujo |
| Titas	Neimontas| Litovujo |
| Albert Stalin	Garrido | Hispanujo |
| Wojciech	Oreszczuk | Pollando |
| Michael Boris Mandirola | Italio |
| Arnaud Lagrange | Francujo |
| Sophie Gawel | Francujo |
| Alejandro Pinos Cabello | Hispanujo |
| *kaŝita* |
| Yousef Hamdi El farouki | Hispanujo |
| *kaŝita* |
| David Ruíz Sánchez | Germanujo |
| Thomas Demeyere | Belgujo |
| Jaime Tapia Zaragoza | Hispanujo |
| Lucía Fernández Barrera | Germanujo|
| Tymoteusz  Smoliński | Pollando |
| Pol Jimenez Isach | Katalunujo |
| Adam Żyłka | Pollando |
| Zofia Żyłka | Pollando |
| Pjotr De Mulder | Belgujo |
| Ana Ribeiro | Germanujo |
| Hamza Salam | Pakistano |
| Ambreen Islam| Pakistano |
| Muhammad Babar Mughal | Pakistano |
| Muhammad Basil Babar | Pakistano |
| Héloise Lellouche | Nederlando |
| Cyril Schultz | Nederlando |
| Yentl-Rose Hemelrijk | Francujo |
| Lea Gavoille | Slovakujo |
| Seohyun Hwang | Sudkoreio |
| Giulia Beltramini | Italio |
| Tabatha Fabri | Belgujo |
| *kaŝita* |
| Etienne Lecointe | Francujo |
| Emma Villemin | Francujo |
| Frédéric Leon Düe | Germanujo |
| *kaŝita* |
| Gobi abyssinie Parfait | Svisujo |
| Ben Indestege | Belgujo |
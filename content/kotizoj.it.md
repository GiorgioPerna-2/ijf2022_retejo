---
title: "Quota d'iscrizione"
---
## Acconti
Affinchè l'iscrizione sia valida, la IEJ deve ricevere un acconto.
L'acconto **minimo** per lo IJF 2024 è di **40€**, o tutta la somma nel caso in cui la somma da pagare sia minore di questa cifra.

Dopo aver ricevuto l'acconto, verrà inviata una conferma sulla disponibilità dei posti: solamente con questa conferma l'iscrizione sarà valida e diverrà ufficiale.

All'arrivo nella sede del festival, ci sarà bisogno di mostrare una prova del proprio acconto e un documento d'identità.

In caso di **ritiro dell'iscrizione**, l'acconto minimo non sarà restituito. Se la cifra pagata come acconto sarà superiore alla soglia minima, allora si avrà diritto al rimborso della somma al netto dell'acconto, a condizione che l'assenza sia stata comunicata entro il 20/03/2023.

Si può **trasferire la propria iscrizione** ad un altro partecipante, a condizione di presentare un documento scritto che attesti il trasferimento; la quota tuttavia verrà adattata in base al nuovo partecipante (periodo/età/paese). Non si possono trasferire più acconti alla stessa persona.


## Quote
Le quote base del festival variano in base a tre parametri, qui di seguito spiegati:

- **Paese di residenza:** Gli stati sono divisi in quattro gruppi a seconda del Prodotto Interno Lordo e della distanza. Le rispettive quote diminuiscono dal primo al quarto gruppo:
   1. Andorra, Austria, Belgio, Catalogna, Città del Vaticano, Danimarca, Finlandia, Francia, Germania, Irlanda, Italia, Lichtenstein, Lussemburgo, Norvegia, Paesi Bassi, Principato di Monaco, Regno Unito, San Marino, Svezia, Svizzera. 
   2. Cipro, Grecia, Islanda, Malta, Portogallo, Repubblica Ceca, Slovacchia, Slovenia, Spagna.
   3. Australia, Brunei, Canada, Croazia, Emirati Arabi Uniti, Estonia, Giappone, Hong Kong, Israele, Kuwait, Lettonia, Lituania, Qatar, Nuova Zelanda, Polonia, Singapore, Stati Uniti, Ungheria. 
   4. Tutti gli altri stati. 

- **Periodi di iscrizione:** Vengono considerati quattro periodi di iscrizione: 
   1. Entro il 20/12/2023 
   2. Dal 21/12/2023 fino al 30 gennaio 2024
   3. Dal 31 gennaio 2024 al 10 marzo 2024
   4. Dall'11 marzo 2024 fino all'inizio del Festival
 

- **Età:** L'età dei partecipanti si divide in quattro categorie, con tariffe che aumentano con l'età. I bambini sotto i 3 anni non pagano la quota.
L'età viene calcolata al primo giorno del festival, che quest'anno sarà il 27/03/2024.

Ricordiamo inoltre che i partecipanti che risiedono in Italia e che non sono membri della Federazione Esperantista Italiana (FEI)) dovranno pagare una somma aggiuntiva, che non è inclusa nella quota calcolata nell'aliĝilo, come spiegato sotto:

**Non membri**
| Partecipante | Età | Somma da pagare |
| --- | --- | --- |
| Ordinario | 25+ | 32€ |
| Giovane | 0 - 24 | 16€ |

L'iscrizione alla FEI può essere effettuata direttamente all'arrivo nella sede del festival; il costo è il seguente:

**Iscrizione alla FEI**
| Membro | Età | Somma da pagare|
| --- | --- | --- |
| Ordinario | 25+ | 30€ |
| Giovane | 0-25 | 15€ |

Tutti coloro che sono membri della FEI dovranno dimostrare di aver pagato la quota d'iscrizione all'associazione.

**Paesi di provenienza 1**
| Età \\ Periodo di iscrizione  | 1  | 2  | 3  | 4  | 
| ----------------- | ---  | ---  | ---  | ---  |
| **3-18** | 225  | 240  | 255  | 267  |
| **19-26** | 240  | 255  | 270  | 282  |
| **27-35** | 272  | 287  | 302  | 314  |
| **36+** | 320  | 335  | 350  | 362  |

**Paesi di provenienza 2**
| Età \\ Periodo di iscrizione | 1 | 2 | 3 | 4 | 
| ----------------- | --- | --- | --- | --- |
| **3-18** | 214 | 229 | 244 | 256 |
| **19-26** | 229 | 244 | 259 | 271 |
| **27-35** | 261 | 276 | 291 | 303 |
| **36+** | 309 | 324 | 339 | 351 |

**Paesi di provenienza 3**
| Età \\ Periodo di iscrizione | 1 | 2 | 3 | 4 | 
| ----------------- | --- | --- | --- | --- |
| **3-18** | 199 | 214 | 229 | 241 |
| **19-26** | 214 | 229 | 244 | 256 |
| **27-35** | 246 | 261 | 276 | 288 |
| **36+** | 294 | 309 | 324 | 336 |
 
**Paesi di provenienza 4**
| Età \\ Periodo di iscrizione | 1 | 2 | 3 | 4 | 
| ----------------- | --- | --- | --- | --- |
| **3-18** | 180 | 195 | 227 | 275 |
| **19-26** | 195 | 210 | 242 | 290 |
| **27-35** | 210 | 225 | 257 | 305 |
| **36+** | 222 | 237 | 269 | 317 |

Le quota riportate sopra sono quelle base, per la permanenza completa durante i 6 giorni. Esse includono: l’alloggio, i pasti (dalla cena del 27 marzo alla colazione del 02 aprile) e la partecipazione al programma del festival. Esse *non* includono gli asciugamani (che possono essere presi sul luogo) e la tassa di soggiorno di 2€ della regione Trentino Alto Adige.

In caso di **soggiorno per meno di 6 giorni**, la quota giornaliera è di **un quinto** della quota base. 

I servizi aggiuntivi che offriamo hanno i seguenti costi:

| Servizio | Prezzo |
| --- | --- |
| Camera tripla | 5€ al giorno | 
| Camera doppia | 10€ al giorno |
| Camera singola | 20€ al giorno |
| Pasti aggiuntivi | 15€ |
| Lettera d'invito (se si hanno meno di 30 anni) | 5€ |
| Lettera d'invito (se si hanno più di 30 anni) | 10€ |

**ATTENZIONE: Le lettere d'invito devono essere richieste il prima possibile a causa delle tempistiche burocratiche.
E' fortemente consigliato che chi richiede la lettera d'invito paghi in anticipo la somma intera, così che sia più probabile l'ottenimento del visto. In caso di non ottenimento del visto, verrà rimborsata la differenza tra la quota totale e l'acconto dovuto.** 

## Sconti
Per l'IJF sono fissati quattro tipi diversi di sconti (oltre a quelli derivanti da borse di studio e altri accordi):
   1. **Contributo al programma:** Riguarda i partecipanti che propongono e gestiranno attività per il programma (riguardante il tema o meno). Solitamente lo sconto è di 10€ per ogni ora di contributo; 
   2. **Collaborazione all'organizzazione:** Riguarda i partecipanti che aiuteranno gli organizzatori per le attività di gestione del festival (trinkejo, gufujo, ecc). Lo sconto in questo caso viene deciso caso per caso; 
   3. **Sostenitori della IEJ:** Riguarda i membri sostenitori della IEJ. Lo sconto è del 10%;
   4. **Sconto di gruppo:** Sconto che si applica ai partecipanti che verranno attraverso un gruppo esperantista locale o in gruppo di almeno 6 persone. Esso varia in base al numero di membri del gruppo. 
   
   Per ulteriori informazioni e accordi, contattaci all'indirizzo iej@esperanto.it. 

---
title: "Ekskursoj"
---

**Vendredon la 29-an de Marto ni havos duontagan ekskurson al Rovereto!**

Rovereto estas historie riĉa urbo, kies originaj radikoj datus al la antikva periodo de la romia kaj kelta kulturoj. Ĝi ludis signifan rolon dum la Mezepoko, kiel komerca kaj kultura centro de la regiono. Poste, ĝi fariĝis parto de la Ŝtato de Venecio, kiu havis grandan influon sur ĝian urban kaj arkitekturan disvolviĝon.
Hodiaŭ, Rovereto estas moderne evoluanta urbo, kies kultura kaj socia vivo estas **ĝoja miksaĵo de tradicio kaj moderneco**. La urbo gajnis reputacion pro sia etoso de kuneco, natura beleco, kaj daŭra kultura agado.

La **Muzeo de Moderna kaj Nuntempa Arto (MART) estas trezoro en la koro de Rovereto**: fondita en 1987, la muzeo estas fokuse dediĉita al la prezento de moderna kaj nuntempa arto, kaj okupas imponan modernan konstruaĵon. 
**Renzo Piano**, mondfama itala arkitekto, estis implikita en la dezajno de la Muzeo MART en Rovereto, kiu estas unu el liaj plej famaj projektoj: ĝia moderna kaj eleganta arkitektura stilo perfekte kongruas kun la kreema spirito de la muzeo. 

La kolektoj de la MART estas gravaj kaj diversaj, kun artoverkoj de renomaj artistoj kaj de novaj talentoj el ĉiuj partoj de la mondo. Krom la ekspoziciajn galeriojn, MART gastigas multajn eventojn, inkluzive de kulturaj prelegoj, muzikaj koncertoj kaj portempaj ekspozicioj. Tio igas ĝin ne nur muzeo, sed vera vivanta kaj ŝanĝiĝanta centro de kultura vivo.

En ĉi tiu kunteksto, vizito al la Muzeo MART estas pli ol simpla renkonto kun arto - ĝi estas sperto de kultura riĉeco kaj kreado, kiu revigas kaj inspiras sian publikon.

![kastelo1](https://iej.esperanto.it/ijf/2024/mart.jpeg)
<img src = "https://iej.esperanto.it/ijf/2024/mart.jpeg" alt="mart">




**Sabaton la 30-an de Marto ni havos tuttagan ekskurson al Trento!**

Trento estas urbo en norda Italio, en la regiono Trentino-Alto Adige/Südtirol, kun proksimume 117.000 loĝantoj. 

Ĝia antikva nomo "Tridentum" eble devenas de la tri montoj (Bondone, Calisio, Marzola) aŭ la tri montetoj (Sant'Agata, San Rocco, Verruca) kiuj ĉirkaŭas la urbon.

La urbo posedas riĉan historion, kiu etendiĝas ekde la antikva Roma periodo. 
Trento estas konata pro siaj kultura diverseco, Universitato, kaj ankaŭ arĥitekturo. La urbo havas belan kaj bone konservitan malnovan urboparton, kiu inkluzivas la katedralon de San Vigilio kaj la kastelon Buonconsiglio.

La **Kastelo Buonconsiglio** (kies nomo laŭlitere signifas "Kastelo de la bona konsilo"), estas la plej granda kaj plej grava monumento de la regiono.
Fortikaĵo el la mezo de la 13-a jarcento, tiama loĝejo de la princoj-episkopoj de Trento ĝis la napoleona epoko, poste kazerno kaj nuntempe muzeo, ĝi prezentas sin kiel formita el tri ĉefaj kernoj, konstruitaj dum malsamaj epokoj. Nepre indas viziti ĝin pro ĝiaj belaj freskoj kaj por ĝui la panoraman vidon de la urbo.

Ĉirkaŭ Trento troviĝas **belaj montoj kaj lagetoj**, kies pejzaĝo estas mirinda. La regiono estas populara inter turistoj pro siaj montoj, promenadoj, skiado, kaj aliaj eksteraj aktivaĵoj.
La regiono ofertas diversajn kuirajn spacialaĵojn, inkluzive de diversaj specoj de fromaĝoj, vinoj, kaj dolĉaĵoj (kiel ekzemple la tradicia strudelo).

Krome, ni ankaŭ havos la ŝancon ekkoni kaj renkonti la **lokajn esperantistojn**, por scii plu pri la vivo en tiu ĉi ĉarma urbo!

Esploru pli kun ni!

![kastelo1](https://iej.esperanto.it/ijf/2024/kastelo1.jpg)
<img src = "https://iej.esperanto.it/ijf/2024/kastelo1.jpg" alt="kastelo1">

![kastelo2](https://iej.esperanto.it/ijf/2024/kastelo2.jpg)
<img src = "https://iej.esperanto.it/ijf/2024/kastelo2.jpg" alt="kastelo2">






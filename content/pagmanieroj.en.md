---
title: "Payment options"
---
## Bank transfer
It is the most used method to transfer sums of money from one current account to another.

Payment must be made to:

- Bank: Intesa San Paolo
- Owner: Italian Esperantist Federation
- IBAN: IT76T0306909606100000131970
- BIC: BCITITMM.

It is necessary to indicate in the reason for payment "IJF 2023 – [Deposit/Balance] – NAME OF THE MEMBER".

## PayPal
One of the most used methods to transfer sums of money via the internet. Creating an account is very simple.

To pay, simply enter your PayPal account, choose the "Send money" option and enter the following payment details:

- E-mail address: iej.financoj@esperanto.it
- Sum: The sum to be transferred (deposit or balance) in euros
- Purchase of: Services.

In the space "Message to the recipient" enter "IJF 2022 – [Antaŭpago/Kotizo] – NOMO DE LA ALIĜINTO".

**ATTENTION: In case of payment via PayPal, IEJ has an additional cost for the service (3.4%+0.35€), which will be charged to the participant at the time of registration on site.**

## Credit card
Through UEA you can pay by credit card to IEJ account. Simply enter the page [https://db.uea.org/alighoj/spagilo.php](https://db.uea.org/alighoj/spagilo.php), enter the IEJ code (ieja-z), write "Alia celo" in the "Pago-celo" box, kaj specify the reason for payment: "IJF 2022 - [Account/Balance] - NAME OF THE MEMBER".

**ATTENTION: In case of payment by credit card, the IEJ has an additional service charge (5%), which will be charged to the participant upon registration on site.**

## UEA account
For those who have an account with the Universal Esperanto Association, it is possible to send money to the IEJ by email to financoj@co.uea.org (copying iej@esperanto.it), indicating your personal account code and the receiver code, i.e. the IEJ (ieja-z).

**ATTENTION: after 03/15/2023 it will no longer be possible to pay with this method.**

## Cash
It is also possible to pay the deposit in cash, specifying the amount paid and the recipient.

It will also be possible to pay the balance in cash upon arrival at the festival, at the time of registration and room assignment.
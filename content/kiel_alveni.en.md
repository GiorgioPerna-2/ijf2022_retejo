---
title: "How to get there"
---
## Location information

* **Name** Villaggio Donna Canfora
* **Address:** Contrada Scinà 89015 PALMI (RC)
* **Coordinates:** 38°24’08.3”N - 15°52’05.4”E
* **Telephone number:** 333 8486812 - 0966 420390
* **Website** info@donnacanfora.it

## How to reach the location
On this page we present some possibilities to reach the venue of the festival.
Consult the following sites for more information and updated timetables:

* [Skyscanner](https://www.skyscanner.it/) (for airplanes)
* [Trenitalia](https://www.trenitalia.com/) (for trains)

The following itineraries are the most recommended for time, distance and cost. Unless otherwise indicated, they are valid for both outward and return journeys

### {{<mdi mdi-airplane>}} By plane
The nearest airport is Lamezia Terme.
However, other options are the airports of Naples and Rome.
Reaching instead the airports of Milan Malpensa, Linate or Orio al Serio, you can continue by taking a train or a bus to Calabria.

### {{<mdi mdi-train>}} By train or bus
To travel by train there are many options for reaching Palmi: using the Frecciarossa trains it is possible to travel from the main Italian cities (Milan, Rome, Naples) to Rosarno, a city which is twenty minutes by train from Palmi.

By choosing other trains, the railway station closest to the Festival venue is that of Gioia Tauro.

Finally, to travel by bus there are many Flixbus services departing from the main Italian cities to reach Gioia Tauro, the city next to Palmi.

### {{<mdi mdi-car>}} By car
Come on, in 2023 you definitely own a phone with a navigator... Are you really reading this? 🙂


---
title: "Pagmanieroj"
---
## Ĝiro
Ĝi estas la plej uzata metodo por transigi monsumojn el iu konto al alia.

La ĝiro estas farenda al:

- Banko: Intesa San Paolo
- Titolisto: Federazione Esperantista Italiana
- IBAN: IT76T0306909606100000131970
- BIC: BCITITMM.

Nepras indiki en la pagkial-loko “IJF 2024 – NOMO DE LA ALIĜINTO”.

## Pajpalo
Unu el la plej uzataj metodoj por transigi monsumojn per interreto. Krei konton estas tre facile kaj rapide.

Por pagi sufiĉas eniri sian konton PayPal, elekti la eblon “Sendu monon” kaj enmeti la subajn informojn:

- Retadreso: iej.financoj@esperanto.it
- Sumo: La transigenda sumo (antaŭpago aŭ elpago) en Eŭroj
- Aĉeto de: Servoj.

En la loko “Mesaĝo por la ricevonto” necesas skribi “IJF 2024 – [Antaŭpago/Kotizo] – NOMO DE LA ALIĜINTO”.

**ATENTU: Okaze de pago per Pajpalo, IEJ havas aldonan elspezon pro la servo (3,4%+0,35€), kiu estos pagigita al la partoprenanto je la momento de la registriĝo surloke.**

## Kreditkarto
Pere de UEA eblas pagi per kreditkarto al la konto de IEJ. Sufiĉas eniri la retpaĝon [https://db.uea.org/alighoj/spagilo.php](https://db.uea.org/alighoj/spagilo.php), indiki la kont-kodon de IEJ (ieja-z), skribi “Alia celo” en la lokon “Pago-celo”, kaj precizigi la celon de la pago: “IJF 2024 – NOMO DE LA ALIĜINTO”.

**ATENTU: Okaze de pago per kreditkarto, IEJ havas aldonan elspezon pro la servo (5%), kiu estos pagigita al la partoprenanto je la momento de la registriĝo surloke.**

## UEA-konto
Por tiuj, kiuj havas konton ĉe la Universala Esperanto Asocio, eblas sendi monon al IEJ pere de retmesaĝo al financoj@co.uea.org (kopie ankaŭ al iej@esperanto.it), indikante la kontkodojn sian kaj de la adresato, nome IEJ (ieja-z).

**ATENTU: post la 10a de Marto 2024 ne plu eblos pagi per tiu ĉi metodo.**

## Kontanta mono
Eblas ankaŭ pagi la antaŭpagon per kontanta mono, indikante la donitan monsumon kaj la ricevinton. 

Kontantmone eblos ankaŭ plenumi la elpagon ĉe la festivalo, en la akceptejo, kiam oni kompletigos la aliĝon kaj estos asignita la ĉambro.

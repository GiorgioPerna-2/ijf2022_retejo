---
title: "Kotizoj"
---
## Antaŭpagoj
Por ke la aliĝo validu, IEJ devos ricevi antaŭpagon. 

La **minimuma** antaŭpago por la IJF 2024 estas **40€**.

Post la enveno de la antaŭpago, sendiĝos konfirmo pri la disponebleco de lokoj: nur je ĉi tiu konfirmo la aliĝo ekvalidos kaj oficialiĝos. 

Kiam oni aliĝos ĉe la kongresejo, necesos prezenti pruvilon de sia antaŭpago kaj identigilon.

Okaze de **retiro je la aliĝo**, la minimuma antaŭpago ne estos redonita. Se la antaŭpagita sumo estos pli alta ol la minimumaj 40€, oni rajtos ricevi repagon de la kroma kvanto, kondiĉe ke la malĉeesto estos oficiale komunikita al IEJ antaŭ la 7a de Marto 2024.

Oni rajtas **transigi sian aliĝon** al alia partoprenonto, kondiĉe ke oni prezentos skribaĵon pruvantan tion; la kotizo tamen alĝustiĝos laŭ la nova aliĝinto (periodo/aĝo/loĝlando). 
Ne eblas transdono de pluraj antaŭpagoj al la sama persono.


## Kotizoj 
La bazaj kotizoj de la festivalo varias laŭ tri parametroj, ĉi sube klarigitaj: 

- **Loĝlando:** La landoj estas disdividitaj en kvar arojn laŭ la Malneta Enlanda Produkto kaj la distanco. La respektivaj kotizoj malpligrandiĝas de la 1-a al la 4-a aro: 
   1. Andoro, Aŭstrujo, Belgujo, Britujo, Katalunujo, Danujo, Finnujo, Francujo, Germanujo, Irlando, Italujo, Liĥtenŝtejno, Luksemburgo, Monaka Princlando, Norvegujo, Nederlando, San-Marino, Svedujo, Svisujo, Vatikanurbo. 
   2. Ĉeĥujo, Grekujo, Hispanujo, Islando, Kipro, Malto, Portugalujo, Slovakujo, Slovenujo. 
   3. Aŭstralio, Brunejo, Estonujo, Honkongo, Hungarujo, Israelo, Japanujo, Kanado, Kataro, Kroatujo, Kuvajto, Latvujo, Litovujo, Nov-Zelando, Polujo, Singapuro, Unuiĝintaj Arabaj Emirlandoj, Usono. 
   4. Ĉiuj aliaj landoj. 

- **Aliĝperiodoj:** Oni konsideras kvar aliĝperiodojn: 
   1. Ĝis la 20a de Decembro 2023 
   2. Ekde la 21a de Decembro 2023 ĝis la 30a de Januaro 2024
   3. Ekde la 31a de Januaro 2024 ĝis la 10a de Marto 2024
   4. Ekde la 11a de Marto 2024 ĝis la komenco de la Festivalo
   
    La respektivaj kotizoj pligrandiĝas laŭ la proksimeco al la festivalo. 

- **Aĝo:** La aĝo de la partoprenontoj dividiĝas en kvar kategoriojn, je fiksitaj tarifoj, kiuj pligrandiĝas kun la aĝo. Infanoj malpli ol 3-jaraĝaj ne pagas kotizon.
La aĝon oni kalkulas je la unua tago de la festivalo, kiu ĉifoje estos la 27a de Marto 2024.

Ni memorigas krome, ke la partoprenantoj loĝantaj en Italujo, kiuj ne estas membroj de la Itala Esperanto-Federacio (IEF), devos pagi kromsumon, kiu ne estas inkluzivita en la kotizo kalkulita de la aliĝilo, kiel sube klarigite: 

**Nemembroj**
| Partoprenanto | Aĝo | Pagenda sumo |
| --- | --- | --- |
| Ordinara | 25+ | 32€ |
| Juna | 0 - 25 | 16€ |

Eblas membrokartiĝo rekte ĉe la festivalejo je la aliĝo, kies kosto estas sube klarite: 

**IEF-membriĝo**
| Membroj | Aĝo | Pagenda sumo|
| --- | --- | --- |
| Ordinara | 25+ | 30€ |
| Juna | 0-24 | 15€ |

Ĉiuj, kiuj estas membroj de IEF bezonas pruvilon, kiu atestas la pagon de la membrokotizo al la asocio. 


**Devenlandoj 1**
| Aĝo \\ Aliĝperiodo | 1 | 2 | 3 | 4 | 
| ----------------- | --- | --- | --- | --- |
| **3-18** | 225 | 240 | 255 | 267 |
| **19-26** | 240 | 255 | 270 | 282 |
| **27-35** | 272 | 287 | 302 | 314 |
| **36+** | 320 | 335 | 350 | 362 |

**Devenlandoj 2**
| Aĝo \\ Aliĝperiodo | 1 | 2 | 3 | 4 |
| ----------------- | --- | --- | --- | --- |
| **3-18** | 214 | 229 | 244 | 256 |
| **19-26** | 229 | 244 | 259 | 271 |
| **27-35** | 261 | 276 | 291 | 303 |
| **36+** | 309 | 324 | 339 | 351 |

**Devenlandoj 3**
| Aĝo \\ Aliĝperiodo | 1 | 2 | 3 | 4 |
| ----------------- | --- | --- | --- | --- |
| **3-18** | 199 | 214 | 229 | 241 |
| **19-26** | 214 | 229 | 244 | 256 |
| **27-35** | 246 | 261 | 276 | 288 |
| **36+** | 294 | 309 | 324 | 336 |

**Devenlandoj 4**
| Aĝo \\ Aliĝperiodo | 1 | 2 | 3 | 4 |
| ----------------- | --- | --- | --- | --- |
| **3-18** | 180 | 195 | 227 | 275 |
| **19-26** | 195 | 210 | 242 | 290 |
| **27-35** | 210 | 225 | 257 | 305 |
| **36+** | 222 | 237 | 269 | 317 |
 


La supraj kotizoj estas tiuj bazaj, nome la kotizoj por tuttempa restado dum 6 tagoj. Ili inkluzivas la loĝadon, la manĝojn (de la vespermanĝo de la 27-a de Marto ĝis la matenmanĝo de la 02-a de Aprilo) kaj partoprenon al la programo de la festivalo.
La kotizo *ne* inkluzivas la bantukojn, kiuj tamen povos esti luprenataj surloke.
Ne estas inkluzivita ankaŭ la turisma impoŝto de 2€, kiun la regiono Trentino Alto Adige pagigas.

Okaze de **restado daŭranta malpli ol 6 tagojn**, la tagkotizo estas por ĉiu tago **kvinono** de la respektiva baza kotizo. 

La pliaj servoj, kiujn oni ofertas, estas montrataj ĉi-sube: 

| Servo | Prezo |
| --- | --- |
| Trilita ĉambro | 5€ tage | 
| Dulita ĉambro | 10€ tage |
| Unulita ĉambro | 20€ tage |
| Pliaj manĝoj | 15€ |
| Invitletero (se oni aĝas malpli ol 30 jarojn) | 5€ |
| Invitletero (se oni aĝas pli ol 30 jarojn) | 10€ |

**ATENTU: La invitleterojn oni petu kiel eble plej rapide pro la burokrataj temponecesoj, kiu rilatas al la petoprocezo. 
Estas ege inde ke kiu petas invitleteron sendu antaŭpage la tutan kotizon, tiel ke estu pli verŝajna la akiro de la vizo. Okaze de neakiro, redoniĝos la diferenco inter la tuta kotizo kaj la endaj antaŭpagoj.** 

## Rabatoj 
Por la IJF 2024 estas antaŭviditaj kvar malsamaj tipoj de rabato (krom tiuj pro la stipendioj kaj ĉiuj aliaj interkonsentoj):
   1. **Kontribuo por la programo:** Koncernas la partoprenantojn, kiuj proponas kaj mastrumos aktivaĵojn por la programo, tema aŭ netema. Kutime la rabato estas po 10€ por ĉiu horo da kontribuo; 
   2. **Kunlaborado al la organizado:** Koncernas la partoprenantojn, kiuj helpos la organizantojn por la agadoj por la mastrumado de la festivalo (trinkejo, gufujo, ktp). La rabato estas ĉi tiam kutime decidata laŭ la specifa kazo; 
   3. **Subteno de IEJ:** Koncernas la subtenantajn membrojn de IEJ. La rabato estas 10%-a; 
   4. **Gruprabato:** Rabato, kiu aplikiĝas al partoprenantoj, kiuj alvenos tra loka Esperanto-grupo aŭ en grupo pli ol 6-homa. Ĝi varias laŭ la numero da grupanoj. 
   
Por pliaj informoj kaj interkonsentoj, kontaktu nin rekte ĉe iej@esperanto.it. 

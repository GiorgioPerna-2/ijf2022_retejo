---
title: "Contacts"
---
For information about the festival:

* E-mail: iej@esperanto.it;
* Telegram group: https://t.me/ijf2022
* Facebook event: https://fb.me/e/1d8aUte5C

For information about the association:

* E-mail: iej@esperanto.it;
* Website: [iej.esperanto.it](https://iej.esperanto.it/);
* Facebook page: [Gioventù Esperantista Italiana – Itala Esperantista Junularo](https://www.facebook.com/IEJesperanto/).


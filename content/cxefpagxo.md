---
#title: "Internacia Junulara Festivalo 2024"
type: homepage
---
Ankaŭ en 2024 okazos la IJF!
La **Internacia Junulara Festivalo (IJF)**, organizita de la Itala Esperantista Junularo (IEJ), estas la plej grava junulara itala evento, kiu okazas dum la Paska periodo.

Ĉijare la Festivalo portos nin en la mirindan regionon **Trentino Alto Adige**, regiono tre alloga por sportaj kaj promenemaj personoj, por bongustuloj kaj kulturŝatantoj, kiu tutcerte proponos al vi nekredeblan restadon.

La urbo kiu gastigos nin estas **Serrada di Folgaria**: vere ĉarma vilaĝo proksime de la bela urbo **Trento**.
 
Dum la paska semajno, de la **27-a de Marto ĝis la 2-a de Aprilo 2024**, ni kuntravivos sep tagojn kune en tiu bela loko.

Kiel ĉiujare, la Internacia Junulara Festivalo elstaras pro la varieco de sia programo,
kutime distingebla en tri **malsamaj kategorioj**:

* **Tema programo**. Kreita ĉirkaŭ la temo elektita de IEJ por ĉiu festivalo, riĉa je prelegoj,
  laborgrupoj kaj seminarioj;
* **Turisma programo**. Forta je la ekskursoj duontaga kaj tuttaga kiuj celas montri al la
  partoprenantoj la plejbonon de norda Italujo;
* **Distra programo**. Malhomogena kaj plurfaceta, konsistas je ĉiu nerilata al la oficiala
  programo, kiel ekzemple ludoj, dancoj, internaciaj vesperoj, koncertoj kaj diskejo
  subtenataj de la trinkejo (por diboĉemaj mojosuloj) kaj de la gufujo, ejo je paco kaj
  malstreĉo por ŝatantoj de babilado kaj varmaj trinkaĵoj.

Grava ebleco por **partopreni preskaŭ senpage** estas pere de TEJO-projekto, kiel klarigite detale ĉi tie: https://www.tejo.org/partoprenanto-seminario-ijf2024/?fbclid=IwAR1b22uBfv5LuJJNFNlDS-SPI5Cn-olESmh63KMRm7CPNkZv8MA1CltC_LA


![projekto](https://iej.esperanto.it/ijf/2024/projekto.png)
<img src = "https://iej.esperanto.it/ijf/2024/projekto.png" alt="projekto">
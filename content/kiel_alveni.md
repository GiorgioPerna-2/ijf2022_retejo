---
title: "Kiel alveni"
---
## Sidejo de la Festivalo
* **Nomo:** Hotel Lares
* **Adreso:** Via Enrico Fermi 44, 38064 Serrada di Folgaria (TN)
* **Koordinatoj:** 45°53’53.7”N - 11°9’21.4”E
* **Telefonnumero:** +39 0464 727129
* **Retadreso** lares@mhhotels.eu

## Kiel alveni
En tiu ĉi paĝo ni prezentas kelkajn eblecojn por atingi la festivalejon.
Konsultu la sekvajn ttt-ejojn por havi pliajn informojn kaj ĝisdatajn horarojn:

* [Skyscanner](https://www.skyscanner.it/) (por la flugoj)
* [Trenitalia](https://www.trenitalia.com/) (por la trajnoj)

La sekvaj itineroj estas la plej konsilindaj laŭ tempo, distanco, komforto kaj
ŝparemo. Krom se la malo estas indikita, ili validas ire kaj revene.

### {{<mdi mdi-airplane>}} Aviadile
La plej proksimaj flughavenoj estas tiuj de Venezia, Treviso, Verona, Bergamo kaj Bolzano.

### {{<mdi mdi-train>}} Trajne kaj buse
La plej granda urbo proksime al Serrada estas Rovereto (TN). Oni povas facile atingi ĝin kaj buse kaj trajne (ĝi situas sur la linio Bologna-Brennero).
El Rovereto estas busoj por atingi kaj rekte kaj nerekte Serrada, kie situas nia Festivalejo.

<!--Ĉi tie la retejo pri bushoroj: LINK ORARI ROVERETO-SERRADA-->

Kaze de bezono de pliaj informoj aŭ sugestoj, senprobleme kontaktu nin ĉe iej@esperanto.it

###  {{<mdi mdi-bike>}} Bicikle
Partoprenu en BEMI-karavano sur bonega biciklovojo tra la valo de Adiĝo. La startloko ankoraŭ ne estas certa, povas esti Balgach (CH), Pfronten (DE), Mittenwald (DE) aŭ iu ajn alia loko, eĉ sude, se vi preferas. 
Kontaktu BEMI nun por kunelekti: bemi-estraro@tejo.org.

### {{<mdi mdi-car>}} Aŭte
Nu, en 2024 vi tutcerte havas poŝtelefonon kun navigilo… Ĉu vi vere estas leganta tie ĉi? 🙂

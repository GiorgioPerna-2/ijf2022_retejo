---
title: "Elenco degli iscritti"
---

| **Iscritto** | **Paese** |
| --- | --- |
| Giovanni De Lucia | Italia |
| Diego Filippo Lotesto | Italia |
| Gabriele Esposito Guido | Italia |
| Giulia Costanzi | Italia |
| Fabian Sambrano Aguilar | Italia |
| Petr Javorik | Repubblica Ceca |
| Vlasta Pištíková | Repubblica Ceca |
| Azadi Mhdjou | Comore |
| Lars Sözüer | Svizzera |
| Mattia Ciceri | Italia |
| Utku Özdemir | Italia |
| Aušrinė	Tamošiūnaitė | Spagna |
| Vittorio	Bignami | Italia |
| *nascosto* | 
| *nascosto* |
| Jack	Fordon | Francia |
| Aida	Čižikitė | Lituania |
| Liudvikas	Medzevičius | Lituania |
| Glorija Elžbieta	Lukaitė | Lituania |
| Pol	Jimenez Isach | Andorra |
| *nascosto* | 
| *nascosto* |
| Miglė	Jančaitytė| Lituania |
| Titas	Neimontas| Lituania |
| Albert Stalin	Garrido | Spagna |
| Wojciech	Oreszczuk | Polonia |
| Michael Boris Mandirola | Italia |
| Arnaud Lagrange | Francia |
| Sophie Gawel | Francia |
| Alejandro Pinos Cabello | Spagna |
| *nascosto* |
| Yousef Hamdi El farouki | Spagna |
| *nascosto* |
| David Ruíz Sánchez | Germanio |
| Thomas Demeyere | Belgio |
| Jaime Tapia Zaragoza | Spagna |
| Lucía Fernández Barrera | Germania|
| Tymoteusz  Smoliński | Polonia |
| Pol Jimenez Isach | Catalogna |
| Adam Żyłka | Polonia |
| Zofia Żyłka | Polonia |
| Pjotr De Mulder | Belgio |
| Ana Ribeiro | Germania |
| Hamza Salam | Pakistan |
| Ambreen Islam| Pakistan |
| Muhammad Babar Mughal | Pakistan |
| Muhammad Basil Babar | Pakistan |
| Héloise Lellouche | Paesi Bassi |
| Cyril Schultz | Paesi Bassi |
| Yentl-Rose Hemelrijk | Francia |
| Lea Gavoille | Slovacchia |
| Seohyun Hwang | Corea del Sud |
| Giulia Beltramini | Italia |
| Tabatha Fabri | Belgio |
| *nascosto* |
| Etienne Lecointe | Francia |
| Emma Villemin | Francia |
| Frédéric Leon Düe | Germania |
| *nascosto* |
| Gobi abyssinie Parfait | Svizzera |
| Ben Indestege | Belgio |

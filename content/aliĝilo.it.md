---
#title: "Modulo d'iscrizione"
---
Per iscriverti al festival, clicca [qui](https://www.shorturl.at/hiwH3)
<!-- 
questo è il link esteso: 
https://docs.google.com/forms/d/1UhC1WtnypgTDUZltBlw7ZwouD50gSSAkwTAHqzF_i1c/edit
-->
Iscriviti subito! Ricorda: chi prima arriva meglio alloggia 😉
